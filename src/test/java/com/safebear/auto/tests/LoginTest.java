package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest{

    @Test
        public void loginTest(){

        // Step 1 ACTION: Open our web application in the browser
        driver.get(Utils.getURL());
        // Step 1 EXPECTED RESULT: Check we are on the Login Page
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page" , "The Login Page didn't open, or the title text has changed");

        // Step 2 ACTION: Enter Username and Password
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");

        // Step 3 ACTION: Press the Login button
        loginPage.clickLoginButton();
        // Step 3 EXPECTED RESULT: check that we're now on the Tools Page
        Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page", "The Login Page didn't open, or the title text has changed");
        // Step 3 EXPECTED RESULT: check the success message is shown
        Assert.assertTrue(toolsPage.getLoginSuccessMessage().contains("Success"));

    }

    @Test
        public void failedloginTest(){

        // Step 1 ACTION: Open our web application in the browser
        driver.get(Utils.getURL());
        // Step 1 EXPECTED RESULT: Check we are on the Login Page
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page" , "The Login Page didn't open, or the title text has changed");

        // Step 2 ACTION: Enter Incorrect Username & Correct Password
        loginPage.enterUsername("tester99");
        loginPage.enterPassword("letmein");

        // Step 3 ACTION: Press the Login button
        loginPage.clickLoginButton();
        // Step 3 EXPECTED RESULT: 'WARNING: Username or Password is incorrect message' appears
        Assert.assertTrue(loginPage.getInvalidLoginMessage().contains("Username or Password is incorrect"));

    }


}
