package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class LogInPageLocators {

    private By usernameLocator = By.id("username");
    private By passwordLocator = By.id("password");
    private By LoginButtonLocator = By.id("enter");
    private By InvalidLogInLocator = By.id("rejectLogin");

}
