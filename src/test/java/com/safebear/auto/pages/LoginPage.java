package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LogInPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {

    LogInPageLocators locators = new LogInPageLocators();
    @NonNull WebDriver driver;

    public String getPageTitle(){
        return driver.getTitle();
    }

    public void enterUsername(String username){
        driver.findElement(locators.getUsernameLocator()).sendKeys(username);
    }

    public void enterPassword(String password){
        driver.findElement(locators.getPasswordLocator()).sendKeys(password);
    }

    public void clickLoginButton(){
        driver.findElement(locators.getLoginButtonLocator()).click();
    }

    public String getInvalidLoginMessage(){
        return driver.findElement(locators.getInvalidLogInLocator()).getText();
    }

}
